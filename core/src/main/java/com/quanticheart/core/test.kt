/*
 * My copyright
 * My licence
 * My licenseSimple
 *
 * (C)2023-02-27 
*/
package com.quanticheart.core

import android.app.Activity
import android.widget.Toast

//
// Created by Jonn Alves on 08/02/23.
//
fun Activity.hello() = Toast.makeText(this, "Hello", Toast.LENGTH_LONG).show()
