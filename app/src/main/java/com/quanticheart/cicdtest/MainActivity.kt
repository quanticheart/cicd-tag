/*
 * My copyright
 * My licence
 * My licenseSimple
 *
 * (C)2023-02-27 
*/
package com.quanticheart.cicdtest

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.quanticheart.core.hello

class MainActivity : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    val tv = findViewById<TextView>(R.id.versionTV)
    tv.text = BuildConfig.VERSION_NAME
    hello()
  }
}
