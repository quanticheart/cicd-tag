/*
 * My copyright
 * My licence
 * My licenseSimple
 *
 * (C)2023-02-27 
*/
package com.quanticheart.cicdtest

import org.junit.Assert.*
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
  @Test
  fun addition_isCorrect() {
    assertEquals(4, 2 + 2)
  }
}
