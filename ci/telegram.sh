#!/bin/bash

## Get matching GitLab user ID and telegram username if available.
USERNAME="User:+$GITLAB_USER_NAME ($GITLAB_USER_ID)"
AUTHOR="Commit by:+$CI_COMMIT_AUTHOR"

STATUS="Status:+$(if [ $CI_JOB_STATUS = "success" ]; then echo "Success"; else echo "Fail"; fi)"

TITLE="Deploy: $1"
START="Start at:+$CI_JOB_STARTED_AT"
NAME="Project:+$CI_PROJECT_NAME"
MSG="Commit:+$CI_COMMIT_MESSAGE"
BRANCH="Branch:+$CI_COMMIT_REF_SLUG"
LINK="Url:+$CI_PROJECT_URL"
PIPELINE="Pipeline:+$CI_PIPELINE_URL"
PIPELINE_ID="Pipeline ID:+$CI_PIPELINE_ID"

TIME="10"
URL="https://api.telegram.org/bot$TELEGRAM_ID/sendMessage"
TEXT="$TITLE%0A$START%0A$STATUS%0A$NAME%0A$BRANCH%0A$MSG%0A$LINK%0A$PIPELINE%0A$PIPELINE_ID%0A%0A$USERNAME%0A$AUTHOR%0A%0A$MESSAGE"

curl -s --max-time $TIME -d "chat_id=$TELEGRAM_ADM_CHAT&disable_web_page_preview=1&text=$TEXT" $URL > /dev/null