#!/bin/bash

STAGE="Stage at:+$CI_JOB_STAGE"
NAME="Project:+$CI_PROJECT_NAME"
BRANCH="Branch:+$CI_COMMIT_REF_SLUG"

TIME="10"
URL="https://api.telegram.org/bot$TELEGRAM_ID/sendMessage"
TEXT="$STAGE%0A$NAME%0A$BRANCH%0A%0A$MESSAGE"
QUERY="chat_id=$TELEGRAM_ADM_CHAT&disable_web_page_preview=1&text=$TEXT"
echo

curl -s --max-time $TIME -d "chat_id=$TELEGRAM_ADM_CHAT&disable_web_page_preview=1&text=$TEXT" $URL > /dev/null

#RESP=$(curl -s --max-time $TIME -d $QUERY $URL)
#echo $RESP