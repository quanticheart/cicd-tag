#!/bin/bash

LINK="Release:+$CI_PROJECT_URL/-/releases/$CI_COMMIT_TAG"
DOWNLOAD="Download:+$CI_PROJECT_URL/-/jobs/$1/artifacts/download"

TIME="10"
URL="https://api.telegram.org/bot$TELEGRAM_ID/sendMessage"
TEXT="$LINK%0A$DOWNLOAD"

curl -s --max-time $TIME -d "chat_id=$TELEGRAM_ADM_CHAT&disable_web_page_preview=1&text=$TEXT" $URL > /dev/null