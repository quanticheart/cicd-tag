# Create CI CD 

- [Repo Documentation](https://quanticheart.gitlab.io/cicd-tag/)

#### Docs Gitlab

- [understanding-principles-of-gitlab-ci-cd-pipelines](https://nexocode.com/blog/posts/understanding-principles-of-gitlab-ci-cd-pipelines/)
- [create-a-release-when-a-git-tag-is-created](https://docs.gitlab.com/ee/user/project/releases/release_cicd_examples.html#create-a-release-when-a-git-tag-is-created)
- [predefined_variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
- [caching](https://docs.gitlab.com/ee/ci/caching/)
- [job_artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html)

#### Image used -> [gitlab-ci-android](https://github.com/jangrewe/gitlab-ci-android)

#### Android
- [creating-a-private-maven-repository-in-gitlab-for-android-libraries](https://medium.com/@emrememil/creating-a-private-maven-repository-in-gitlab-for-android-libraries-fb2676bc82c1)
- [creating-a-private-maven-repository-for-android-libraries-on-gitlab](https://proandroiddev.com/creating-a-private-maven-repository-for-android-libraries-on-gitlab-91137c402777)
- ** release config -> [android-gitlab-ci-cd-sign-deploy-release](https://medium.com/android-news/android-gitlab-ci-cd-sign-deploy-3ad66a8f24bf)

#### Articles

- [gitlab-automatic-releases-with-ci-cd-pipelines](https://www.philipp-doblhofer.at/en/blog/gitlab-automatic-releases-with-ci-cd-pipelines/)
- [android-ci-cd-getting-started](https://mochadwi.medium.com/android-ci-cd-getting-started-c3dc936806c8)
- [creating-a-private-maven-repository-in-gitlab-for-android-libraries](https://medium.com/@emrememil/creating-a-private-maven-repository-in-gitlab-for-android-libraries-fb2676bc82c1)
- [android-gitlab-ci-setup](https://blog.canopas.com/android-gitlab-ci-setup-c3618b8ef0db)
- [creating-a-private-maven-repository-for-android-libraries-on-gitlab](https://proandroiddev.com/creating-a-private-maven-repository-for-android-libraries-on-gitlab-91137c402777)
- [release-artifacts-using-gitlab-cicd](https://crypt.codemancers.com/posts/2021-08-31-release-artifacts-using-gitlab-cicd/)
- [Variáveis secretas não estão presentes em compilações "apenas tags"](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/50284)
- [add-a-cicd-variable-to-a-project](https://docs.gitlab.cn/14.0/ee/ci/variables/README.html#add-a-cicd-variable-to-a-project)
- [job-variables](https://www.perforce.com/manuals/gitswarm/ci/yaml/README.html#job-variables)
- [job-variables](http://mpegx.int-evry.fr/software/help/ci/variables/predefined_variables.md)


#### Examples .sh
- [gitlab-ci-telegram-bot](https://gitlab.com/kuzzzminskii/gitlab-ci-telegram-bot)
- [gitlab-telegram-alert](https://github.com/suchimauz/gitlab-telegram-alert/blob/main/entrypoint.sh)
- [Telegram-iOS](https://github.com/TelegramMessenger/Telegram-iOS/blob/master/.gitlab-ci.yml)

# Libs 

#### Spotless

- [github](https://github.com/diffplug/spotless/tree/main/plugin-gradle#ktlint)
- [ktlint](https://github.com/pinterest/ktlint/tree/0.48.2)

#### gradle-android-git-version

- [github](https://github.com/gladed/gradle-android-git-version)

#### gradle-versions-plugin

- [github](https://github.com/ben-manes/gradle-versions-plugin)

#### dokka

- [github](https://github.com/Kotlin/dokka)

